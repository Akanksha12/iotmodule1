# INTERNET OF THINGS (IOT)

## Consumer IOT vs Industrial IOT
![image1](https://www.i-scoop.eu/wp-content/uploads/2017/01/The-difference-between-the-Industrial-Internet-of-Things-and-Consumer-Internet-of-Things-as-depicted-by-Vector-Software-source-courtesy-Vector-Software.jpg)

* Collecting data from Physical devices and sharing  on internet ( wireless mode) can be termed as Internet  Of Things

*  May be used at 
> * Consumer level  ( personal devices are connected to 

internet) .
> * Industial level     ( industrial machinaries and devices are connected to intetnet) 

# INDUSTRIAL IOT
  
![image](https://l82od4c3ddp2xkios1wuwoqf-wpengine.netdna-ssl.com/wp-content/uploads/2017/03/Industry4point0.jpg)

### INDUSTRY 3.0 

* Data stores in data base and represented in excels.

###  INDUSTRY 3.0 ARCHITECTURE
  
   VS

###  INDUSTRY 4.0 ARCHITECTURE


![image123](https://blog.ebv.com/wp-content/uploads/2017/02/Transition-Industry-3.0-4.0-680x218.jpg)

*  **IN 3.0 INDUSTRY**
 SENSORS (at different point of industry )=> field bus (collects data) => PLC's => SCADA & ERP (Stores data)
 Stored in excel CSV's and rarely get plotted real time graphs or chart. 

   * At the bottom of the automation pyramid,  the field level consists of sensors and actuator.
   * The top levels, the MES/SCADA and ERP layers, handle regulation as well as process and production planning.
 * **IN 4.0 INDUSTRY** 
 Data from controller to cloud viaindustry 4.0 protocols
   * more open and flexible and can support new requirements such as strong vertical communication.
    * this model considers more number of smart devices and sensors at the field level.

 ### INDUSTRY 3.0 COMMUNICATION PROTOCOLS


 * The Protocols that are used by Sensors to send data to PLC's are called Field Bus.

| Industry 3.0 protocol | industry 4.0 protocol |
| ------ | ------ |
|1. Optimized to send data to central server | 2. Optimized to send data from controller to cloud |
| 2.  Ex. **Modbus, Profenet,EtherCat,CanOpen** |2. Ex. **MQTT,HTTP, {Rest full API},AMQP**|

 ##### INDUSTRY 4.O = INDUSTRY 3.0 + INTERNET

 ###### What happens when devices commits to internet?

 * Industries get access to **more data** about their own product and their own internal systems and greater ability to make changes.

 ![123](https://iot-industrial-devices.com/wp-content/uploads/2020/01/iot-edge-2020.png)

# CHALLENGES with Industry 4.0 upgrades and ways to overcome them:

* CHALLENGES..
![img](https://www.jml-industrie.com/images/divers/blog/2019/JML_4.0_challenges.png)

### Solution

* Data is gathered from industry 3.0 devices, sensors etc .without changes in industry 3.0 devices  and then send the data to cloud using industry 4.0 devices.

* NIST cyber security guidelines framework can be used to prevent Cyber attacks.

## STEPS FOR DOING SO...

STEP 1. CONVERT 3.0 PROTOCOLS --> 4.O PROTOCOLS 

STEP 2. OVERCOMING CHANGES :- 
a. expensive hardware. 
b.lack of documentation. 
c. PLC protocol . 


STEP 3. WAYS OF CONVERSION 
We use **library** to convert 3.0 devices to 4.0 cloud


STEP 4. **Road Map** for making own Industrial project.
* Identified most popular Industry 3.0 device.
* Study Protocols
* let data from 3.0 devices.
* Send the data to cloud for Industry 4.0.

### GROUND WORK
* IDENTIFY 3.0 DEVICES WHICH CAN BE UPGRADED TO 4.0 
EX: PLC, industrial Meters and Industial valve
* Now that data is sent to internet analyse it using its tools.

## IOT TSDB tools
* Store your data in time series database.
 * Prometheus 
 * influx  PB
 * Grafand
 * Things board

 ## IOT Dash Board

 ==> View all the data in dash board

 ## IOT PLATFORM

*  ==> AWS IOT
* ==> Google IOT
*  ==>Asure IOT

## CUT ALERT

 DATA USING THESE PLATFORM

 * ZIAPER

 * TWILIO

 
 #  HOW DATA LOOKS INSIDE THE CLOUD
![image](https://www.altairsmartworks.com/images/uploads/get_started/cpanel.png)




 


